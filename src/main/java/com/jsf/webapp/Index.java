package com.jsf.webapp;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class Index implements Serializable {

	private static final long serialVersionUID = 1604098944528298254L;
	private static final org.apache.logging.log4j.Logger log = org.apache.logging.log4j.LogManager.getLogger(Index.class);
	
	@PostConstruct
	public void init() {
		log.info("Initializing Index");
	}

	public String getTime() {
		return ZonedDateTime.now().toString();
	}

}
